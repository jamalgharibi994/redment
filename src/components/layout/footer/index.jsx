import React from "react";
import "./Footer.css"
import logo from "../../../assets/images/layout/logo1.png"
import ScrollToTop from "./components/backToTop";

function Footer () {
    return (
        <>
            <footer className="footer-main">
                <div className="footer-info">
                    <div className="footer-info-left">
                        <img className="footer-logo" src={logo} alt={logo}/>
                        <p className="footer-info-p">Your idea blended with the latest technology</p>
                    </div>
                    <div className="footer-info-right">
                        <div className="info-email">
                            <p className="info-email-p1">CONNECT WITH US</p>
                            <p className="info-email-p2">jamalgharibi40@gmail.com</p>
                        </div>
                        <span className="footer-info-line"></span>
                        <div className="info-phone">
                            <p className="info-phone-p1">CONNECT FOR CAREER</p>
                            <p className="info-phone-p2">09370672474</p>
                        </div>
                    </div>
                </div>
                <div className="footer-section-down">
                    <div>
                        <p className="copyright">All Right Reserved by jamal gharibi.</p>
                    </div>
                    <div className="footer-section-down-share">
                        <a target="_blank" rel="noreferrer" className="footer-instagram" href="https://instagram.com">
                            <svg width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M15.5154 1.91699H7.48459C3.99625 1.91699 1.91667 3.99658 1.91667 7.48491V15.5062C1.91667 19.0041 3.99625 21.0837 7.48459 21.0837H15.5058C18.9942 21.0837 21.0738 19.0041 21.0738 15.5157V7.48491C21.0833 3.99658 19.0038 1.91699 15.5154 1.91699ZM11.5 15.2187C9.44917 15.2187 7.78167 13.5512 7.78167 11.5003C7.78167 9.44949 9.44917 7.78199 11.5 7.78199C13.5508 7.78199 15.2183 9.44949 15.2183 11.5003C15.2183 13.5512 13.5508 15.2187 11.5 15.2187ZM17.1733 6.59366C17.1254 6.70866 17.0583 6.81408 16.9721 6.90991C16.8763 6.99616 16.7708 7.06324 16.6558 7.11116C16.5408 7.15908 16.4163 7.18783 16.2917 7.18783C16.0329 7.18783 15.7933 7.09199 15.6113 6.90991C15.525 6.81408 15.4579 6.70866 15.41 6.59366C15.3621 6.47866 15.3333 6.35408 15.3333 6.22949C15.3333 6.10491 15.3621 5.98033 15.41 5.86533C15.4579 5.74074 15.525 5.64491 15.6113 5.54908C15.8317 5.32866 16.1671 5.22324 16.4738 5.29033C16.5408 5.29991 16.5983 5.31908 16.6558 5.34783C16.7133 5.36699 16.7708 5.39574 16.8283 5.43408C16.8763 5.46283 16.9242 5.51074 16.9721 5.54908C17.0583 5.64491 17.1254 5.74074 17.1733 5.86533C17.2213 5.98033 17.25 6.10491 17.25 6.22949C17.25 6.35408 17.2213 6.47866 17.1733 6.59366Z" fill="#6D7280"/>
                            </svg>
                        </a>
                        <a target="_blank" rel="noreferrer" className="footer-linkedin" href="https://www.linkedin.com">
                            <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fillRule="evenodd" clipRule="evenodd" d="M21.621 12.8309V20.815H16.9588V13.3554C16.9588 11.4905 16.3177 10.2084 14.6276 10.2084C13.3455 10.2084 12.5879 11.0825 12.2382 11.8984C12.1216 12.1898 12.0634 12.5978 12.0634 13.0057V20.815H7.40109C7.40109 20.815 7.45937 8.16862 7.40109 6.88651H12.0634V8.86796C12.0634 8.86796 12.0634 8.92623 12.0051 8.92623H12.0634V8.86796C12.7044 7.93551 13.7534 6.53683 16.2594 6.53683C19.2899 6.53683 21.621 8.57657 21.621 12.8309ZM2.62253 0.18457C1.04901 0.18457 0 1.23358 0 2.57397C0 3.91436 0.990734 4.96339 2.56425 4.96339H2.62253C4.25433 4.96339 5.24506 3.91437 5.24506 2.57397C5.18679 1.23358 4.19605 0.18457 2.62253 0.18457ZM0.233118 20.815H4.83712V6.88651H0.233118V20.815Z" fill="#6D7280"/>
                            </svg>
                        </a>
                    </div>
                </div>
                <ScrollToTop />
            </footer>
        </>
    );
}
export default Footer;