import React, { useState } from "react";
import {NavLink} from "react-router-dom";
import logo from "../../../assets/images/layout/logo1.png"
import logoRes from "../../../assets/images/layout/logo-res.png"
import "./Header.css"

function Header() {
    const data = [{
        liClass : 'Nav-item',
        navClass: 'Nav-link',
        data :[{
            navTo: 'Services',
            navContent : 'Services'
        },{

            navTo: 'Services',
            navContent : 'Services'
        }]
    }]
    const [menu, setMenu] = useState(false);
    return (
        <header>
            <section className="header-area" id="header">
                <div className="logo">
                        <a href="/">
                            <img src={logo} alt="logo"/>
                        </a>
                    </div>
                <ul className="Navbar-nav ">
                        <li className="Nav-item">
                            <NavLink className="Nav-link" to="/Services">Services</NavLink>
                        </li>
                        <li className="Nav-item">
                            <NavLink className="Nav-link" to='/Products'>Products</NavLink>
                        </li>
                        <li className="Nav-item">
                            <NavLink className="Nav-link" to='/CaseStudies'>Case Studies</NavLink>
                        </li>
                        <li className="Nav-item">
                            <NavLink className="Nav-link" to='/Company'>Company</NavLink>
                        </li>
                        <li className="Nav-item">
                            <NavLink className="Nav-link" to='/Blog'>Blog</NavLink>
                        </li>
                        <li className="Nav-item">
                            <NavLink className="Nav-link" to='/GetPricing'>Get Pricing</NavLink>
                        </li>
                    </ul>
                <div className="responsive-menu">
                    <button className="menu-btn">HIRE US</button>
                    <span className="line"></span>
                    <a className="trigger" onClick={() => setMenu(!menu)}>
                        <svg width="40" height="40" viewBox="0 0 40 40" fill="#000000" xmlns="http://www.w3.org/2000/svg">
                            <path d="M34.999 28.3334L4.99902 28.3334" stroke="#000000" strokeWidth="1.5" strokeLinecap="round"/>
                            <path d="M34.999 20L4.99902 20" stroke="#000000" strokeWidth="1.5" strokeLinecap="round"/>
                            <path d="M34.999 11.6666L4.99902 11.6666" stroke="#000000" strokeWidth="1.5" strokeLinecap="round"/>
                            <path d="M34.999 28.3334L4.99902 28.3334" stroke="#000000" strokeWidth="2.5" strokeLinecap="round"/>
                            <path d="M34.999 20L4.99902 20" stroke="#000000" strokeWidth="2.5" strokeLinecap="round"/>
                            <path d="M34.999 11.6666L4.99902 11.6666" stroke="#000000" strokeWidth="2.5" strokeLinecap="round"/>
                        </svg>
                    </a>
                    <div className="responsive" style={menu ? {display:"block"}:{display:"none"}}>
                        <div className="res-header">
                            <div className="logoRes">
                                <a href="/">
                                    <img src={logoRes} alt="logoRes"/>
                                </a>
                            </div>
                            <svg onClick={() => setMenu(!menu)} className="multiplication" width="47" height="46" viewBox="0 0 47 46" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M29.2343 28.6569L17.9205 17.3432" stroke="#374151" strokeWidth="2.5" strokeLinecap="round" strokeLinejoin="round"/>
                                <path d="M29.2342 17.3432L17.9205 28.6569" stroke="#374151" strokeWidth="2.5" strokeLinecap="round" strokeLinejoin="round"/>
                            </svg>

                        </div>
                        <ul className="responsive-Navbar-nav">
                            <li className="responsive-Nav-item">
                                <NavLink onClick={() => setMenu(!menu)} className="Nav-link-res" to='/Company'>Company</NavLink>
                            </li>
                            <li className="responsive-Nav-item">
                                <NavLink onClick={() => setMenu(!menu)} className="Nav-link-res" to="/Services">Services</NavLink>
                            </li>
                            <li className="responsive-Nav-item">
                                <NavLink onClick={() => setMenu(!menu)} className="Nav-link-res" to='/CaseStudies'>Case Studies</NavLink>
                            </li>
                            <li className="responsive-Nav-item">
                                <NavLink onClick={() => setMenu(!menu)} className="Nav-link-res" to='/Products'>Products</NavLink>
                            </li>
                            <li className="responsive-Nav-item">
                                <NavLink onClick={() => setMenu(!menu)} className="Nav-link-res" to='/Blog'>Blog</NavLink>
                            </li>
                            <li className="responsive-Nav-item">
                                <NavLink onClick={() => setMenu(!menu)} className="Nav-link-res" to='/Contact'>Contact</NavLink>
                            </li>
                        </ul>
                    </div>
                </div>
            </section>
        </header>
    );
}
export default Header;