import React from "react";
import "./NotFound.css"

function NotFound () {
    return (
        <>
            <section className="main-NotFound">
                <div className="NotFound-Contact">
                    <p>page not found !</p>
                </div>
            </section>
        </>
    );
}
export default NotFound;