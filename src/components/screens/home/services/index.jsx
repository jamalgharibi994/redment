import React from "react";
import "./Services.css"
import Software from "../../../../assets/images/home/software.png"
import product from "../../../../assets/images/home/product.png"
import product2 from "../../../../assets/images/home/product2.png"


function Services () {
    return (
        <>
            <section id="Services">
                <section className="services-main">
                    <div className="services-info">
                        <p className="work-what">What we can for you</p>
                        <p className="work-title">Services <span className="work-circle"></span></p>
                        <p className="work-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga fugiat in iste officia, optio perspiciatis ratione recusandae sequi tempora, ullam voluptatibus voluptatum! Aspernatur distinctio esse incidunt libero qui quis, tenetur.</p>
                        <a className="work-do" href="#">See what we do</a>
                    </div>
                    <div className="Services-service">
                        <div className="service">
                            <img src={Software} alt="Software"/>
                            <div className="service-info">
                                <p className="service-info-p1">Software Development</p>
                                <p className="service-info-p2">Deliver you product right on every platform</p>
                            </div>
                        </div>
                        <div className="service">
                            <img src={product} alt="product"/>
                            <div className="service-info">
                                <p className="service-info-p1">Product Design</p>
                                <p className="service-info-p2">Reach for beauty and usability at once</p>
                            </div>
                        </div>
                        <div className="service">
                            <img src={product2} alt="product2"/>
                            <div className="service-info">
                                <p className="service-info-p1">Product Discovery</p>
                                <p className="service-info-p2">Find out how to turn your idea into reality</p>
                            </div>
                        </div>
                    </div>
                </section>
            </section>
        </>
    );
}

export default Services;