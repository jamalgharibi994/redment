import React from "react";
import "./Expertise.css"
import Hotel from "../../../../assets/images//home/hotel.png"
import Logistic from "../../../../assets/images/home/logistic.png"
import Financial from "../../../../assets/images/home/financial.png"
import Organizational from "../../../../assets/images/home/organizational.png"


function Expertise () {
    return (
        <>
            <section className="expertise-main" id="Expertices">
                <div className="expertise-info">
                    <p className="expertise-p1">Industries we expert in</p>
                    <p className="expertise-title">Expertise <span className="expertise-circle"></span></p>
                    <p className="expertise-description">Redment excels in the following areas and operates at least one successful platform</p>
                </div>
                <div className="expertise-pics">
                    <div className="expertise-pics-left">
                        <img className="hotel" src={Hotel} alt="Hotel" />
                        <div className="hotel-Background">
                            <p className="experience-title">Hoteling</p>
                            <p className="years-experience">3.5 years of experience</p>
                        </div>
                    </div>
                    <div className="expertise-pics-right">
                        <div className="">
                            <img className="logistic" src={Logistic} alt="Logistic" />
                            <div className="logistic-Background">
                                <p className="experience-title">Logistics</p>
                                <p className="years-experience">3 years of experience</p>
                            </div>
                        </div>
                        <div className="expertise-pics-right-down">
                            <div className="financial-box">
                                <img className="financial" src={Financial} alt="Financial" />
                                <div className="financial-Background">
                                    <p className="experience-title">Financial</p>
                                    <p className="years-experience">2.5 years of experience</p>
                                </div>
                            </div>
                            <div className="organizational-box">
                                <img className="organizational" src={Organizational} alt="Organizational" />
                                <div className="organizational-Background">
                                    <p className="organizational-experience-title">Organizational agility</p>
                                    <p className="years-experience">1 years of experience</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
}
export default Expertise;