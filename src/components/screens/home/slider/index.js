import React from "react";
import Slider from "react-slick";
import "./Slider.css"

import slid1 from "../../../../assets/images/home/Rectangle1.png"
import slid2 from "../../../../assets/images/home/Rectangle2.png"
import slid3 from "../../../../assets/images/home/Rectangle3.png"


function Sliders () {

    const settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true
    };

    return (
        <>
            <section className="slider-main" id="Testimonials">
                <div className="slider-info">
                    <p className="slider-expert">What our customers have to sey</p>
                    <p className="slider-title">Testimonials<span className="slider-circle"></span></p>
                    <p className="slider-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga fugiat in iste officia, optio perspiciatis ratione recusandae sequi tempora, ullam voluptatibus voluptatum! Aspernatur distinctio esse incidunt libero qui quis, tenetur.</p>
                </div>
                <div className="slider">
                    <Slider {...settings}>
                        <div>
                            <img className="slider-img" src={slid1} alt="slid1"/>
                        </div>
                        <div>
                            <img className="slider-img" src={slid2} alt="slid2"/>
                        </div>
                        <div>
                            <img className="slider-img" src={slid3} alt="slid3"/>
                        </div>
                    </Slider>
                </div>
            </section>
        </>
    );
}

export default Sliders;