import React from "react";
import { Tab } from '@headlessui/react'
import "./Technologies.css"
import Microsoft_Azure_Logo from "../../../../assets/images/home/Microsoft_Azure_Logo.png"
import Nginx_logo from "../../../../assets/images/home/Nginx_logo.png"
import Jenkins_logo_with_title from "../../../../assets/images/home/Jenkins_logo_with_title.png"

function Technologies () {
    return (
        <>
            <section className="technologies-main" id="Technologies">
                <div className="technologies-info">
                    <p className="technologies-expert">We are expert in this technologies</p>
                    <p className="technologies-title">Our Technologies <span className="technologies-circle"></span></p>
                    <p className="technologies-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga fugiat in iste officia, optio perspiciatis ratione recusandae sequi tempora, ullam voluptatibus voluptatum! Aspernatur distinctio esse incidunt libero qui quis, tenetur.</p>
                </div>
                <div className="technologies-tab">
                    <Tab.Group>
                        <Tab.List className="tech-tabs">
                            <Tab className="tech-tab">DevOps</Tab>
                            <Tab className="tech-tab">Database</Tab>
                            <Tab className="tech-tab">Back-end</Tab>
                        </Tab.List>
                        <Tab.Panels className="tech-panels">
                            <Tab.Panel className="tech-panel"> <img src={Microsoft_Azure_Logo}/> </Tab.Panel>
                            <Tab.Panel className="tech-panel"> <img src={Jenkins_logo_with_title}/> </Tab.Panel>
                            <Tab.Panel className="tech-panel"> <img src={Nginx_logo}/> </Tab.Panel>
                        </Tab.Panels>
                    </Tab.Group>
                </div>
            </section>
        </>
    );
}

export default Technologies;