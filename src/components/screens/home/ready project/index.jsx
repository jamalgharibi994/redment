import React from "react";
import "./ReadyProject.css"

function ReadyProject () {
    return (
        <>
            <section className="ReadyProject-main" id="ReadyProject">
                <p className="ReadyProject-Question">Ready to start your project?</p>
                <a href="#StartProject" className="ReadyProject-button">Start your project now</a>
            </section>
        </>
    );
}

export default ReadyProject;
