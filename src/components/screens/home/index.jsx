import React from "react";
import "./Home.css"

import Tab from "./tab";
import Services from "./services";
import Work from "./work";
import Expertices from "./expertise";
import ReadyProject from "./ready project";
import Technologies from "./technologies";
import StartProject from "./Start project";
import Sliders from "./slider";


function Home () {
    return (
        <>
            <section className="main">
                <div className="profile">
                    <h1 className="profile-title">We create <p className="profile-title-p">digital experiences </p>that excite and inspire</h1>
                    <p className="profile-detail">Redment is software development company</p>
                    <a href="#StartProject" className="profile-btn">let's discuss your project</a>
                </div>
            </section>
            <Tab />
            <Services />
            <Work />
            <Expertices />
            <ReadyProject />
            <Technologies />
            <Sliders />
            <StartProject />
        </>
    );
}

export default Home;
