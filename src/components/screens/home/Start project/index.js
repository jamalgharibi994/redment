import React from "react";
import {useFormik} from "formik";
import "./StartProject.css"



function StartProject () {
    const formik = useFormik({
        initialValues : {
            name :'',
            email : '',
            Phone : '',
            message: ''
        },
        validate : (values) => {
            let errors = {};

            if(values.name === ""){
                errors.name = "field cannot be empty";
            }else if(values.name.length < 5){
                errors.name = "field cannot be less than 5 characters"
            }

            if(values.email === ""){
                errors.email = "field cannot be empty";
            }else if (! /^[A-Z0-9._%+-]+@[A-z0-9*-]+\.[A-Z]{2,4}$/i.test(values.email)){
                errors.email = "field cannot be less than 5 characters"
            }

            if(values.Phone === ""){
                errors.Phone = "field cannot be empty";
            }else if(values.Phone.length < 11){
                errors.Phone = "field cannot be less than 11 characters"
            }

            if(values.message === ""){
                errors.message = "field cannot be empty";
            }else if(values.message.length < 5){
                errors.message = "field cannot be less than 5 characters"
            }

            return errors;

        },
        onSubmit : (values) => {
            console.log("Submit ==> ", values);
        }

    })
    return (
        <>
            <section className="start-project-main" id="StartProject">
                <div className="start-project-info">
                    <p className="start-project-expert">Let's get</p>
                    <p className="start-project-title">Start a project<span className="start-project-circle"></span></p>
                    <p className="start-project-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga fugiat in iste officia, optio perspiciatis ratione recusandae sequi tempora, ullam voluptatibus voluptatum! Aspernatur distinctio esse incidunt libero qui quis, tenetur.</p>
                </div>
                <div className="project-form">
                    <form className="form" onSubmit={formik.handleSubmit}>
                        <div className="form-row">
                            <input placeholder="Full name"
                                   name="name"
                                   {...formik.getFieldProps('name')}
                            />
                            {formik.touched.name && formik.errors.name ? <span>{formik.errors.name}</span> :null}
                        </div>
                        <div className="form-row">
                            <input placeholder="Corporate E-mail"
                                   name="email"
                                   {...formik.getFieldProps('email')}
                            />
                            {formik.touched.email && formik.errors.email ? <span>{formik.errors.email}</span> :null}
                        </div>
                        <div className="form-row">
                            <input placeholder="Phone" type="tel"
                                   name="Phone"
                                   {...formik.getFieldProps('Phone')}
                            />
                            {formik.touched.Phone && formik.errors.Phone ? <span>{formik.errors.Phone}</span> :null}
                        </div>
                        <br/>
                        <div className="form-text">
                                    <textarea placeholder="please describe your project"
                                              name="message"
                                              {...formik.getFieldProps('message')}
                                    />
                            {formik.touched.message && formik.errors.message ? <span>{formik.errors.message}</span> :null}
                        </div>
                        <button className="btn" type="submit">Send request</button>
                    </form>

                </div>
            </section>
        </>
    );
}

export default StartProject;
