import React from "react";
import "./Tab.css"
import img7 from "../../../../assets/images/home/img7.png"
import img56 from "../../../../assets/images/home/img56.png"
import img29 from "../../../../assets/images/home/img29.png"


function Tab () {
    return (
        <>
            <section className="main-tab" id="About">
                <div className="tab">
                   <div className="target">
                       <p className="number">1</p>
                       <a className="target-title" href="#About">
                           <p className="p1">About</p>
                           <p className="p2">Redment</p>
                       </a>
                   </div>
                    <div className="target">
                        <p className="number">2</p>
                        <a className="target-title" href="#Services">
                            <p className="p1">Our</p>
                            <p className="p2">Services</p>
                        </a>
                    </div>
                    <div className="target">
                        <p className="number">3</p>
                        <a className="target-title" href="#Expertices">
                            <p className="p1">Domain</p>
                            <p className="p2">Expertices</p>
                        </a>
                    </div>
                    <div className="target">
                        <p className="number">4</p>
                        <a className="target-title" href="#works">
                            <p className="p1">our</p>
                            <p className="p2">works</p>
                        </a>
                    </div>
                    <div className="target">
                        <p className="number">5</p>
                        <a className="target-title" href="#Technologies">
                            <p className="p1">Our</p>
                            <p className="p2">Technologies</p>
                        </a>
                    </div>
                    <div className="target">
                        <p className="number">6</p>
                        <a className="target-title" href="#Testimonials">
                            <p className="p1">Our</p>
                            <p className="p2">Testimonials</p>
                        </a>
                    </div>
                    <div className="target">
                        <p className="number">7</p>
                        <a className="target-title" href="#StartProject">
                            <p className="p1 Blog">StartProject</p>
                        </a>
                    </div>
               </div>
                <div className="tab-content">
                    <h2>Web and Mobile app</h2>
                    <h2>development company</h2>
                    <p className="tab-content-p">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores dolor ea, eaque ex excepturi explicabo, impedit iste nam nemo nisi quae quas quasi qui ratione soluta tenetur vel!</p>
                    <div className="statistics">
                        <div className="statistics-content">
                            <img src={img7} alt="img7"/>
                            <p>Years on the market</p>
                        </div>
                        <div className="statistics-content">
                            <img src={img56} alt="img56"/>
                            <p>Projects completed</p>
                        </div>
                        <div className="statistics-content">
                            <img src={img29} alt="img29"/>
                            <p>Business expertises</p>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
}

export default Tab;
