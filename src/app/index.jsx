import React from "react";
import "./App.css";

import { BrowserRouter, Routes, Route } from "react-router-dom";
import loadable from '@loadable/component';
import homeBackground from "../assets/images/main/home-background.png";
import Shadow from "../assets/images/main/Shadow.png";
import Header from "../components/layout/header";
import Footer from "../components/layout/footer";

// new routes whit @loadable/component package for lazy loading
const Home    = loadable(() => import('../components/screens/home'))
// const Services    = loadable(() => import('./Services/Services'))
// const Products    = loadable(() => import('./Products/Products'))
// const CaseStudies = loadable(() => import('./CaseStudies/CaseStudies'))
// const Blog        = loadable(() => import('./Blog/Blog'))
// const GetPricing  = loadable(() => import('./GetPricing/GetPricing'))
const NotFound    = loadable(() => import('../components/screens/notfound'))

function App() {
    return (
        <BrowserRouter>
            <Header />
            <div className="App">
                <img className="homeBackground" src={homeBackground} alt="homeBackground"/>
                <img className="Shadow" src={Shadow} alt="Shadow"/>
                <Routes>
                    <Route path="/" element={<Home />} />
                    {/*<Route path="/Services" element={<Services />} />*/}
                    {/*<Route path="/Products" element={<Products />} />*/}
                    {/*<Route path="/CaseStudies" element={<CaseStudies />} />*/}
                    {/*<Route path="/Company" element={<Company />} />*/}
                    {/*<Route path="/Blog" element={<Blog />} />*/}
                    {/*<Route path="/GetPricing" element={<GetPricing />} />*/}
                    <Route path="*" element={<NotFound />}/>
                </Routes>
            </div>
            <Footer />
        </BrowserRouter>
    );
}
export default App;